package com.yalo.whatsapp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Main {
	static WebDriver driver = new ChromeDriver();

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
		driver.get("https://whatsapp.yalochat.com/demo?utm_source=website5.0&utm_medium=WhatsAppDemo#prueba-rapida");
		driver.manage().window().maximize();

		userToAbleToStart();
		errorMessages();
		enrollSuccessfully();

		driver.quit();
	}

	public static void userToAbleToStart() {
		// Next
		driver.findElement(By.className("pre-onboarding-button-next")).click();
		driver.findElement(By.className("pre-onboarding-button-next")).click();
		driver.findElement(By.className("pre-onboarding-button-next")).click();
		driver.findElement(By.className("pre-onboarding-button-next")).click();

		// Back
		driver.findElement(By.cssSelector(".first-notification-button-next:nth-child(1)")).click();
		// Start Now
		driver.findElement(By.className("pre-onboarding-button-finish")).click();
	}

	public static void errorMessages() {
		Select countries = new Select(driver.findElement(By.className("react-phone-number-input__country-select")));

		driver.findElement(By.id("name")).sendKeys("Yesi");
		// Country
		countries.selectByValue("GF");
		driver.findElement(By.id("phone")).sendKeys("33357914");
		driver.findElement(By.className("first-notification-button-submit")).click();
		if (!driver.findElements(By.xpath("//*[contains(text(),'Enter a valid phone number')]")).isEmpty()) {
			// 10 digits
			countries.selectByValue("MX");
			driver.findElement(By.id("phone")).clear();
			driver.findElement(By.id("phone")).sendKeys("3335791458");
		}
	}

	public static void enrollSuccessfully() {
		// Hit Enroll
		driver.findElement(By.className("first-notification-button-submit")).click();

		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Confirm')]")));

		// Continue disable
		if (!driver.findElement(By.cssSelector(".first-notification-button-next:nth-child(2)")).isEnabled()) {
			driver.findElement(By.className("first-notification-button-submit")).click();
			// Continue enable
			if (driver.findElement(By.cssSelector(".first-notification-button-next:nth-child(2)")).isEnabled()) {
				driver.findElement(By.cssSelector(".first-notification-button-next:nth-child(2)")).click();
			}
		}

	}

}
